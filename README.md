As the premier dance studio in North County San Diego, SEDMA offers ballet dance classes, contemporary dance classes, jazz dance classes, hip hop dance classes, and lyrical dance classes. Private music lessons are also available in Piano, Voice, Guitar, Acoustic and Electric, Violin and Ukelele.

Address: 1635 S Rancho Santa Fe Rd, #203, San Marcos, CA 92078, USA

Phone: 760-410-1999

Website: [https://sanelijodance.com](https://sanelijodance.com)
